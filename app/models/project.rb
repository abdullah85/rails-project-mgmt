class Project < ApplicationRecord
    has_many :boards

    validates :title, presence: true
    validates :description, presence: true, length: { minimum: 10 }
end
