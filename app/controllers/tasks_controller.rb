class TasksController < ApplicationController
  def show
    @task = Task.find(params[:id])
    @board = @task.board
    @project = @board.project
    @projectPath = "/projects/" + @project.id.to_s
    @boardPath = @projectPath+"/boards/" + @board.id.to_s
  end

  def edit
    @task = Task.find(params[:id])
  end
  
  def update
    @task = Task.find(params[:id])
    if(@task.update(task_params))
      redirect_to @task
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def new
    @task = Task.new
  end
  
  def create
    # See guide below for details on debugging (found it in Gemfile)
    # https://guides.rubyonrails.org/debugging_rails_applications.html#inspecting-variables
    # debugger
    @board = Board.find(params[:board_id])
    @task = @board.tasks.create(task_params)
    if(@task.save)
      redirect_to @task
    else
      render :edit, status: :unprocessable_entity
    end
  end

  private
    def task_params
      params.require(:task).permit(:title, :description)
    end
end
