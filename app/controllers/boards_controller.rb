class BoardsController < ApplicationController
  def index
    @project = Project.find(params[:project_id])
    @boards = Board.where('project_id':@project.id)
  end

  def show
    @board = Board.find(params[:id])
    @tasks = @board.tasks
    @project = @board.project
  end

  def create
    @project = Project.find(params[:project_id])
    @board = @project.boards.create(board_params)
    redirect_to project_path(@project)
  end

  def edit
    @board = Board.find(params[:id])
    @project = @board.project
  end

  def update
    @board = Board.find(params[:id])

    if @board.update(board_params)
      redirect_to @board
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @board = Board.find(params[:id])
    @project = @board.project
    @board.destroy

    redirect_to @project, status: :see_other
  end
  private
    def board_params
      params.require(:board).permit(:title, :description)
    end
end
