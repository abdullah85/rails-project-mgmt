Rails.application.routes.draw do
  root "projects#index"

  resources :projects do
    resources :boards,  only: [:index, :new, :create]
  end

  resources :boards, only: [:show, :edit, :update, :destroy] do
    resources :tasks, only: [:index, :new, :create]
  end

  resources :tasks, only: [:show, :edit, :update, :destroy]

  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
